package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/tidwall/gjson"

	"github.com/xanzy/go-gitlab"
)

var (
	opsGitlabNetAPIURL = "https://ops.gitlab.net/api/v4/"
	chefRepoProjectID  = 139
)

func getFileFromGit(roleFile string) string {

	repositoryFullPath := "roles/" + roleFile + ".json"

	opsGitlabNet := gitlab.NewClient(nil, os.Getenv("GITLAB_OPS_API_TOKEN"))
	opsGitlabNet.SetBaseURL(opsGitlabNetAPIURL)

	rawFileOptions := &gitlab.GetRawFileOptions{
		Ref: gitlab.String("master"),
	}
	file, _, err := opsGitlabNet.RepositoryFiles.GetRawFile(chefRepoProjectID, repositoryFullPath, rawFileOptions)
	if err != nil {
		return ""
	}

	return fmt.Sprint(fmt.Sprintf("%s", file))

}

func main() {

	chefRoles := flag.String("chef-roles", "pre-base-be-sidekiq,pre-base", "comma separated list of chef roles to search for the value, leftmost is highest priority")
	chefAttribute := flag.String("chef-attribute", "default_attributes.omnibus-gitlab.gitlab_rb.external_url", "path of attribute to retrieve")

	flag.Parse()
	if os.Getenv("GITLAB_OPS_API_TOKEN") == "" {
		log.Fatal("GITLAB_OPS_API_TOKEN is unset, can't authenticate")
	}

	for _, role := range strings.SplitN(*chefRoles, ",", -1) {
		chefJSON := getFileFromGit(role)
		attributeOut := gjson.Get(chefJSON, *chefAttribute).String()
		if attributeOut != "" {
			os.Stdout.WriteString(attributeOut)
			os.Exit(0)
		}
	}

	os.Exit(100)

}
